from sys import exit
from roxana import Roxana, RoxanaConfigError


if __name__ == '__main__':
	try:
		roxana_instance = Roxana('./etc/config.json')
	except RoxanaConfigError as e:
		print("Roxana can not be started due to the following reason: ", e.message, " Error code: ", e.string_code, ".")
		exit(1)
	
	roxana_instance.run()