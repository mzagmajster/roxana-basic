from os.path import exists, abspath
from datetime import datetime
from getpass import getuser
import re, logging, pyttsx3
from simplejson import load, dumps
from speech_recognition import Microphone, Recognizer, UnknownValueError

from roxana.core import RoxanaConfigError, RoxanaSignals
from roxana.config import ROX_CODE_CONFIG_PROD, ROX_CODE_CONFIG_DEV
from roxana.actions.download_subtitles import DownloadSubtitles
from roxana.actions.play_movie import PlayMovie


class Roxana(RoxanaSignals):
	USER_ERROR_MESSAGE = "Invalid input. To see all available actions say: 'Roxana help'."
	def _validate_instance_config(self):
		if not exists(self._config_path):
			raise RoxanaConfigError('Path to Roxana config file does not exist.', 'ROX_1')

		self._config_path = abspath(self._config_path)

		with open(self._config_path) as f:
			self._config = load(f)

		# For now just check if we have main list and at least one action we are allow to trigger.
		if not 'actions' in self._config:
			raise RoxanaConfigError('Basic structure of main configuration file is invalid.', 'ROX_2')

	def _validate_action_config(self, schema):
		errors = schema.validate()
		return list()

	def _get_trigger(self):
		i = 0
		while i < len(self._config['actions']):
			for trigger in self._config['actions'][i]['triggers']:
				# Return action index and trigger string.
				yield i, trigger
			i += 1
		return ''

	def _update_config(m, c):
		i = 0
		while i < len(self._config['actions']) and self._config['actions'][i]['module'] != m:
			i += 1

		if i == len(self._config['actions']):
			return {}

		self._config['actions'][i].update(c)

		return self._config['actions'][i]

	def _get_config_index(self, m):
		i = 0
		while i < len(self._config['actions']) and self._config['actions'][i]['module'] != m:
			i += 1

		if i == len(self._config['actions']):
			return -1

		return i

	def __init__(self, config_path, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self._config_path = config_path
		self._config = dict()
		self._validate_instance_config()
		self._shutdown = False
		self._last_action_index = -1
		self._user = getuser()

		# Std input.
		self._std_input_data = None

		# Speech recognition stuff (input).
		self._microphone = Microphone()
		self._recognizer = Recognizer()
		self._speech_audio = None
		self._speech_data = ''

		# Text to speech stuff (output).
		self._engine = pyttsx3.init()
		self._engine.setProperty('voice', 'mb-en1+f1')
		self._engine.setProperty('rate', 145)

		# Code config settings.
		self._code_config = dict()
		if self.is_development_mode():
			self._code_config.update(ROX_CODE_CONFIG_DEV)
		else:
			self._code_config.update(ROX_CODE_CONFIG_PROD)

	def is_development_mode(self):
		return self._config.get('application_mode', 'production') == 'development'

	def is_for_roxana(self):
		return self._speech_data.startswith('roxana')

	def is_core_action(self, action):
		return not len(action['module'])

	def is_running(self):
		return not self._shutdown

	def get_last_speech_data(self):
		try:
			with self._microphone as source:
				self._speech_audio = self._recognizer.listen(source)
				self._speech_data = self._recognizer.recognize_google(self._speech_audio)
				self._speech_data = self._speech_data.lower()
		except UnknownValueError:
			self._speech_data = ''
			return 'ROX_UNRECOGNIZABLE_SPEECH'

		return 'ROX_SPEECH_DETECTED'

	def match_actions(self):
		# Renove "roxana" from input.
		i = self._speech_data.find(' ')
		if i == -1:
			return None
		i += 1
		input_text = self._speech_data[i:]

		for action_index, action_trigger in self._get_trigger():
			match_obj = re.fullmatch(action_trigger, input_text)
			self._responder.send(show=match_obj, level=logging.DEBUG)
			if match_obj is not None:
				self._responder.send(show=match_obj.groupdict(), level=logging.DEBUG)
				self._last_action_index = action_index

				# Populate action object with configuration we got from speech.
				action_config_obj = dict(self._config['actions'][action_index])
				action_config_obj['config'].update(match_obj.groupdict())

				return action_config_obj

		return None

	def roxana_speaking_receiver(self, sender, **kwargs):
		date_string = datetime.now().strftime('%Y-%m-%d %H:%M')
		meta_text = "[" + date_string + "]Roxana: "
		print(meta_text, end='', flush=True)

		# Decide wheter or not app actually show/say given message.
		level = kwargs.get('level', self._code_config['logging_default_level'])
		if level < self._code_config['logging_level']:
			return None

		if 'say' in kwargs:
			print(kwargs['say'])

		if 'show' in kwargs:
			print("")
			print(kwargs['show'])
			print("-" * 80)

		if 'say' in kwargs:
			self._engine.say(kwargs['say'])
			self._engine.runAndWait()

	def roxana_speech_detector_receiver(self, sender, **kwargs):
		date_string = datetime.now().strftime('%Y-%m-%d %H:%M')
		meta_text = "[" + date_string + "]" + self._user + "[" + str(len(self._speech_data)) + "]: {0}"
		print(meta_text.format(kwargs['message']))

	def roxana_std_input_receiver(self, sender, **kwargs):
		meta_text = self._user + "[INPUT]: "
		print(meta_text, end='', flush=True)
		self._std_input_data = input()

	def execute(self):
		# TODO: Improve.
		if self._config['actions'][self._last_action_index]['triggers'][0] == 'quit':
			self._shutdown = True
		elif self._config['actions'][self._last_action_index]['triggers'][0] == 'help':
			self._responder.send(show=dumps(self._config, indent=4, sort_keys=True))

		return 'ROX_ACTION_EXECUTED'

	def initialize_action(self, action):
		if self.is_core_action(action):
			return self

		class_var = DownloadSubtitles
		if action['module'] == 'play_movie':
			class_var = PlayMovie
		
		schema = class_var.get_config_schema(
				raw_json=dumps(action['config'])
			)
		errors = self._validate_action_config(schema)
		if len(errors):
			self._responder.send(say="Validation of config for module failed.", level=logging.INFO)
			return None

		obj = class_var(action['config'])

		return obj

	def run(self):
		self._responder.connect(self.roxana_speaking_receiver)
		self._responder.send(show="Config file in use: {0}".format(self._config_path))

		self._responder.send(say="Initializing. Please be quiet for a moment.")
		# Adjust ambient as quickly as possible.
		with self._microphone as source:
			self._recognizer.adjust_for_ambient_noise(source)

		self._std_inputer.connect(self.roxana_std_input_receiver)

		if len(self._user) == 0:
			self._responder.send(say="Please input your name and press enter.")
			self._std_inputer.send()
			self._user = self._std_input_data
			self._responder.send(say="Thank you.")

		self._speech_detector.connect(self.roxana_speech_detector_receiver)
		self._responder.send(say="Initialized. I am listening.")

		# Main loop vars.
		self._speech_data = ''
		status = ''
		action_config_obj = None

		while self.is_running():
			if not len(self._speech_data) and self._code_config['trigger_source'] == 'std_input':
				self._responder.send(show="Input valid trigger for the action you want to trigger.")
				self._std_inputer.send()
				self._speech_data = self._std_input_data
				status = 'ROX_SPEECH_DETECTED'
			elif not len(self._speech_data):
				status = self.get_last_speech_data()

			if status != 'ROX_SPEECH_DETECTED':
				self._responder.send(say=self.__class__.USER_ERROR_MESSAGE)
				# Cleanup.
				self._speech_data = ''
				status = ''
				action_config_obj = None
				continue

			self._speech_detector.send(message=self._speech_data)

			# Speech was detected but check if it is relevant.
			if not self.is_for_roxana():
				# Cleanup.
				self._speech_data = ''
				status = ''
				action_config_obj = None
				continue

			# Execute action.
			# We need to check for action config like this because of sequence actions.
			if action_config_obj is None:
				action_config_obj = self.match_actions()
				if action_config_obj is None:
					# Cleanup.
					self._speech_data = ''
					status = ''
					action_config_obj = None
					self._responder.send(say=self.__class__.USER_ERROR_MESSAGE)
					continue

			action_obj = self.initialize_action(action_config_obj)
			
			action_status = action_obj.execute()
			if action_status == 'ROX_ACTION_EXECUTED':
				self._responder.send(say="Done.")
			elif action_status == 'ROX_ACTION_TERMINATED':
				self._responder.send(say="Could not execute the action successfully.")
			elif action_status not in self.__class__.ACTION_STATUSES:
				self._responder.send(say="Action status unknown. Status: ", show=action_status)

			# Offer to play video for which we already downloaded.
			if action_config_obj['module'] == 'download_subtitles' and (action_status == 'ROX_ACTION_EXECUTED' or action_status == 'ROX_ACTION_WARNING'):
				output = action_obj.output()
				# Play latest movie config.
				play_movie_index = self._get_config_index('play_movie')
				action_config_obj = self._config['actions'][play_movie_index]
				action_config_obj['config']['files'].extend(output['media_records'])
				
				self._speech_data = 'roxana play latest movie'
				status = 'ROX_SPEECH_DETECTED'
			else:
				# Cleanup.
				self._speech_data = ''
				status = ''
				action_config_obj = None

		self._responder.send(say="Have a nice day.")






