from blinker import signal


class RoxanaConfigError(Exception):
	def __init__(self, message, string_code):
		super().__init__()
		self.message = message
		self.string_code = string_code


class RoxanaConfigSchema(object):
	def __init__(self):
		pass
	
	def validate(self):
		return list()

	def get_config(self):
		return dict()


class RoxanaSignals(object):
	ACTION_STATUSES = [
		'ROX_ACTION_EXECUTED', 
		'ROX_ACTION_TERMINATED',
		'ROX_ACTION_WARNING'
	]

	def __init__(self, *args, **kwargs):
		# Signals.
		self._responder = signal('roxana-speaking')
		self._speech_detector = signal('roxana-speech-detector')
		self._std_inputer = signal('roxana-std-inputer')


class RoxanaAction(RoxanaSignals):
	@classmethod
	def get_config_schema(cls):
		raise NotImplementedError

	def __init__(self, *args, **kwargs):
		super().__init__(*args, **kwargs)

	def execute(self):
		pass