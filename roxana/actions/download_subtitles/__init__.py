from operator import itemgetter
from os import listdir, stat
from os.path import exists, isfile
from babelfish.language import Language
from subliminal import region
from subliminal.core import scan_video, download_best_subtitles, save_subtitles
from subliminal.video import Movie, Episode
from dogpile.cache.exception import RegionAlreadyConfigured

from roxana.core import RoxanaConfigSchema, RoxanaAction


class DownloadSubtitlesConfigSchema(RoxanaConfigSchema):
	def __init__(self, raw_json):
		super().__init__()
		self._raw_json = raw_json
		self._config = dict()

	def validate(self):
		return list()

	def get_config(self):
		return dict()


class DownloadSubtitles(RoxanaAction):
	@classmethod
	def get_config_schema(cls, *args, **kwargs):
		return DownloadSubtitlesConfigSchema(kwargs['raw_json'])

	def _get_directories(self):
		content = listdir(self._config['media_folders'][0])

		for c in content:
			p = self._config['media_folders'][0] + '/' + c
			if isfile(p):
				continue

			self._available_folders.append((stat(p).st_mtime, c))
			self._available_folders.sort(key=itemgetter(0), reverse=True)

	def _get_video_details(self):
		rating_criteria = dict()

		all_parameters = [
			'record',
			'year',
			'season',
			'episode'
		]

		for p in all_parameters:
			if self._config[p] is not None:
				rating_criteria.update({p: 0})

		return rating_criteria

	def _check_video_file(self, path, set_instance=False):
		if not isfile(path):
			return ['not_a_file']

		parts = path.split('.')
		if parts[-1] not in self._config['video_extensions']:
			return ['invalid_extension']

		try:
			video = scan_video(path)
		except ValueError:
			return ['local_filename_parsing_failed']

		# Check if it matches the parameters.
		details = self._get_video_details()
		name = video.name.lower()

		# Names check.
		name_parts = self._config['record'].split(' ')
		i = 0
		j = 0
		while i < len(name_parts):
			j = name.find(name_parts[i], j)
			if j == -1:
				return ['name_does_not_match']
			j += 1
			i += 1 

		details.update({'record': 100})

		year = video.year
		if 'year' in details and year == self._config['year']:
			details.update({'year': 100})

		if isinstance(video, Episode):
			season = video.season
			episode = video.episode

			if 'season' in details and season == self._config['season']:
				details.update({'season': 100})

			if 'episode' in details and episode == self._config['episode']:
				details.update({'episode': 100})

		# We can now calculate the score but, its really not necessary with current configuration.
		# If we came so far we already know that score is > than 0.

		# We set instance if we know that the next step will be downloading the subs.
		if set_instance:
			self._video = video
		else:
			self._media_records.append(path)

		return []

	def _fill_media_records(self):
		for mod_time, af in self._available_folders:
			path = self._config['media_folders'][0] + '/' + af
			content_with_videos = listdir(path)
			for c in content_with_videos:
				potential_video_path = path + '/' + c
				print(potential_video_path)
				errors = self._check_video_file(potential_video_path)
				if not len(errors):
					# We found first match, good enough for now.
					return True
					# TODO: Check all relevant records.
		return False

	def __init__(self, config, *args, **kwargs):
		super().__init__(args, kwargs)
		self._config = config
		try:
			self._region = region.configure('dogpile.cache.memory')
		except RegionAlreadyConfigured as e:
			pass

		self._video = None

		self._languages = set()
		for l in self._config['languages']:
			self._languages.add(Language(l))

		self._subtitles = list()

		self._media_records = list()
		# For the most part this should be empty at init time.
		self._media_records.extend(self._config['media_records'])

		# Helper vars.
		self._available_folders = list()


	def execute(self):
		self._get_directories()
		self._fill_media_records()

		records_count = len(self._media_records)
		i = 0
		if not records_count:
			self._responder.send(say="No video found.")
			return 'ROX_ACTION_TERMINATED'
		elif records_count > 1:
			# Remove possible duplicates.
			self._media_records = list(set(self._media_records))
			records_count = len(self._media_records)
			if records_count > 1:
				log_string = ""
				while i < records_count:
					log_string += str(i + 1) + " - " + self._media_records[i] + "\n"
					i += 1

				self._responder.send(say="More then one video still matches the criteria. Auto selecting first entry for now.", show=log_string)
				i = 0
				# TODO: If there are more choices at the end show a numbered list and offer user a selection.

		# Download and save subtitles.
		self._check_video_file(self._media_records[i], True)
		self._subtitles = download_best_subtitles([self._video], self._languages)
		if not len(self._subtitles):
			self._responder.send(say="No subtitles found.")
			return 'ROX_ACTION_TERMINATED'  # We cannot continue.

		saved_subtitles = save_subtitles(self._video, self._subtitles[self._video])
		if not len(saved_subtitles):
			self._responder.send(say="No new subtitles were downloaded.")
			return 'ROX_ACTION_WARNING'

		return 'ROX_ACTION_EXECUTED'

	def output(self):
		return dict(media_records=self._config['media_records'])

