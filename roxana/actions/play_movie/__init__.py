import subprocess, sys

from roxana.core import RoxanaConfigSchema, RoxanaAction


class PlayMovieConfigSchema(RoxanaConfigSchema):
	def __init__(self, raw_json):
		super().__init__()
		self._raw_json = raw_json
		self._config = dict()

	def validate(self):
		return list()

	def get_config(self):
		return dict()


class PlayMovie(RoxanaAction):
	@classmethod
	def get_config_schema(cls, *args, **kwargs):
		return PlayMovieConfigSchema(kwargs['raw_json'])

	def __init__(self, config, *args, **kwargs):
		super().__init__(*args, **kwargs)
		self._config = config

	def execute(self):
		self._responder.send(say="Enjoy the movie.")

		# TODO: Make it work on Windows.
		opener = 'open' if sys.platform == 'linix' else 'xdg-open'
		subprocess.call([opener, self._config['files'][0]])
		sys.exit(0)
		return 'ROX_ACTION_EXECUTED'