import logging


ROX_CODE_CONFIG_PROD = {
	'logging_level': logging.INFO,
	'logging_default_level': logging.INFO,
	'trigger_source': 'speech'  # speec, _std_inputer
}

ROX_CODE_CONFIG_DEV = {
	'logging_level': logging.DEBUG,
	'logging_default_level': logging.INFO,
	'trigger_source': 'speech'
}