# Roxana Basic

An attempt to implement simple speech based task (action) runner.

## Getting Started


### Prerequisites

Python3 is required to run this succesfully. Project was developed and tested on Linux (Ubuntu 18.04). So in addition to Python packages below you need to install some system packages as well.

```
apt-get update && apt-get install espeak
```

### Installing

It is recommanded that you use at least virtualenv to install requirements.

Install requirements

```
pip install -r requirements.txt
```

Place ```config.json``` inside ```etc``` directory and place the following config to the JSON file (modify it so it suits your needs of course)

```
{
	"application_mode": "development",
	"actions": [
		{
			"module": "",
			"triggers": [
				"help"
			],
			"config": {}
		},
		{
			"module": "",
			"triggers": [
				"quit"
			],
			"config": {}
		},
		{
			"module": "download_subtitles",
			"triggers": [
				"^download subtitles for (?P<record>.*?) ((season (?P<season>\\d+)) (episode (?P<episode>\\d+)))",
				"^download subtitles for (?P<record>.*?) (year (?P<year>\\d+))"
			],
			"config": {
				"media_folders": [
					// Strings. Absolute path to directory where you have movies.
				],
				"languages": [
					"eng"
				],
				"video_extensions": [
					"mp4",
					"avi",
					"mov",
					"flw",
					"wmv",
					"mkv"
				],
				"subtitles_extensions": [
					"srt"
				],

				// This parameters are given later when user speaks.
				"media_records": [],
				"record": null,
				"season": null,
				"episode": null,
				"year": null
			}
		},
		{
			"module": "play_movie",
			"triggers": [
				"play latest movie"
			],
			"config": {
				"media_folders": [
					// Strings. Absolute path to directory where you have movies.
				],
				"files": [
					// Strings. Absolute path(s) to specific media file.
				]
			}
	]
}
```

Comments in the above content are given so you know what exactly goes there. When you are moving this to JSON file, remove the comments.

## Running the tests

@todo

## Deployment

@todo

## Changelog

@todo

## Documentation

@todo

## Built With

* [Python 3](http://www.dropwizard.io/1.0.2/docs/) - Programming language
* [Pyttsx3](https://maven.apache.org/) - Text to speech package
* [SpeechRecognition](https://rometools.github.io/rome/) - Speech to text package.
* [Subliminal](https://subliminal.readthedocs.org/) - Action: Download subtitles

## Contributing

Please read ```CONTRIBUTING.md``` for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the tags on this repository. 

## Authors

Initial content for this project was provided by Matic Zagmajster. For more information please see ```AUTHORS``` file.

## License

This project is licensed under the MIT License - see the ```LICENSE``` file for details.

